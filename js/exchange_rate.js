 //Global variables:
 let urlObject=new Object();
 //localStorageHistory saves both current and old row.
 let localStorageHistory=[];
 //history is just used for the session storage. After the page is reloaded, its contetns are gone.
 let history=[];
 //showedHistory checks if the table showing the history is displayed or not. It is used to render the table in a correct way.
 let showedHistory=false;
document.addEventListener("DOMContentLoaded",function(){
    
    createOptions();
    let convertButton=document.getElementById("convert-btn");
    let showHistoryButton=document.getElementById("history-btn");
    let clearButton=document.getElementById("clear-btn");
    //When the page is loaded, the values in the current local storage are appended to the localStorageHistory variable.
    localStorageHistory=JSON.parse(localStorage.getItem("currency-history"));
    //EventListeners
    convertButton.addEventListener("click",updateURLObject);
    showHistoryButton.addEventListener("click",showHistory);
    clearButton.addEventListener("click",clearTable);
});

//Using the provided JSON file, this function creates the options for both of the select tags.
function createOptions(){
    let fromCurrency=document.getElementById("base-currency");
    let toCurrency=document.getElementById("to-currency");
    //By using fetch, we can access the file.
    fetch("../json/currency.json").then(response=>response.json()).then(function(data){
        for(let i=0;i<Object.values(data).length;i++){

            let option=document.createElement("option");
            //The name of the currency is what we use to display the options.
            option.innerHTML=Object.values(data)[i].name;
            //The code of the currency is set to be the value of its corresponding option
            option.setAttribute("value",Object.values(data)[i].code);

            let option2=document.createElement("option");
            option2.innerHTML=Object.values(data)[i].name;
            option2.setAttribute("value",Object.values(data)[i].code);

            //This if statement set Canadian Dollar as the default selected option.
            if (Object.values(data)[i].code=="CAD") {
                option.selected=true;
                option2.selected=true;
            }
            fromCurrency.appendChild(option);
            toCurrency.appendChild(option2);
        }
    });
}

//This function builds an object by receiving its data from the selected options. At the end, it creates an absloute url which will be later
//sent to the api in oder to convert a currency to another currency.
function updateURLObject(){
    urlObject.mainURL=`https://api.exchangerate.host/convert`;
    urlObject.fromQry=document.getElementById("base-currency").value;
    urlObject.toQry=document.getElementById("to-currency").value;
    urlObject.amountQry=document.getElementById("amount").value
    urlObject.absoluteURL=function(){
        return `${Object.values(urlObject)[0]}?from=${Object.values(urlObject)[1]}&to=${Object.values(urlObject)[2]}&amount=${Object.values(urlObject)[3]}`;
    }
    //sendToAPI is called in order to do the convrsion using the current urlObject.
    sendToAPI();
}
//This function calculates all the desired values using an api. The absolute url which was created in the pervious function is now used to fetch
//data from the currency api. All the desired retrieved values are stored in an object and later, this object is stored in an array.
//This array is used as the parameter of the tableRender() so the program can render the whole tabel using the fetched values.
function sendToAPI(){
    fetch(urlObject.absoluteURL()).then(value=>value.json()).then(data=>{
        const d = new Date();
        //I created an object called valuesObject. Each key of this object represents a column of the table.
        let valuesObject=new Object();
        valuesObject.from=data.query.from;
        valuesObject.to=data.query.to;
        valuesObject.rate=data.info.rate;
        valuesObject.amount=data.query.amount;
        valuesObject.result=data.result;
        valuesObject.date=data.date+` ${d.getHours()}:${d.getMinutes()}:${d.getSeconds()}`;
        if (localStorage.getItem("currency-history")!=null) {
            localStorageHistory=JSON.parse(localStorage.getItem("currency-history"));
         }
        else{
            localStorageHistory=[];
        }
        //I push the created object to the history[].
        history.push(valuesObject);
        //I also push the created object to the localStorageHistory[] because when a row is added on the screen, it is also added to the local storage.
        localStorageHistory.push(valuesObject);
        //localStorageHistory[] is sent to the local storage because it is the array that contains both new and old rows.
        localStorage.setItem("currency-history",JSON.stringify(localStorageHistory));
        //If the user had already clicked on the show history button, then the the table with both new and old values are displayed.
        if(showedHistory){
            tableRender(localStorageHistory);
        }else{
            tableRender(history);
        }
        
    });
}

//tableRender renders the whole table given its parameter. In this
function tableRender(history){
    //To make sure it is empty and/or reset it 
    document.getElementById("table-article").innerHTML="";
    let table=document.createElement("table");
    let thead=document.createElement("thead");
    let tbody=document.createElement("tbody");
    let tr=document.createElement("tr");
    
    let columnText=["from","to","rate","amount","payment","date"];
    for (let thCounter = 0; thCounter < 6; thCounter++) {
        let th=document.createElement("th");
        th.innerHTML=columnText[thCounter];
        tr.appendChild(th);
    }
    thead.appendChild(tr);
    for (let iterator = 0; iterator < history.length; iterator++) {
        let tr =document.createElement("tr");
        for (let tdCounter = 0; tdCounter < 6; tdCounter++) {
            let td=document.createElement("td");
            if (tdCounter==0) {
                td.innerHTML=history[iterator].from;
            }else if(tdCounter==1){
                td.innerHTML=history[iterator].to;
            }else if(tdCounter==2){
                td.innerHTML=history[iterator].rate;
            }else if(tdCounter==3){
                td.innerHTML=history[iterator].amount;
            }else if(tdCounter==4){
                td.innerHTML=history[iterator].result;
            }else{
                td.innerHTML=history[iterator].date;
            }
            tr.appendChild(td);
        }
        tbody.appendChild(tr);
    }
    table.appendChild(thead);
    table.appendChild(tbody);
    document.getElementById("table-article").appendChild(table);
}
//This function clears the current displayed table and replace it with the corresponding table in the local storage.
function showHistory(){
    document.getElementById("table-article").innerHTML="";
    if(localStorageHistory != null){
        //showedHistory=true meaning the user had clicked on the button show history
        showedHistory=true;
        //I use tableRender function and as its parameter, I give the array holding all the values in the local storage.
        tableRender(localStorageHistory);
    }
}
//This function clears everything.
function clearTable(){
    localStorage.removeItem("currency-history");
    history=[];
    localStorageHistory=[];
    document.getElementById("table-article").innerHTML="";
}